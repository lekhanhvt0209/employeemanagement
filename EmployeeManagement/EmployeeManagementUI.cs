﻿public class EmployeeManagementUI
{
    public EmployeeManagement employeeManagement { get; set; }

    public EmployeeManagementUI()
    {
        // đối tượng này dùng để quản lý và lưu trữ nhân viên
        employeeManagement = new EmployeeManagement();
    }

    public void PrintEmployees()
    {

        Employee[] employees = employeeManagement.GetEmployees();
        // TODO: Hiển thị thông tin của nhân viên
        // Định dạng in ra: Tên: [name], Tuổi: [age], Chức vụ: [position], Lương: [salary]
        Console.WriteLine("Danh sách thông tin của nhân viên");
        foreach (Employee employee in employees)
        {
            int count = 0;
            if (employee != null)
            {
                Console.WriteLine(string.Format($"Tên: {employee.Name}, Tuổi: {employee.Age}, Chức vụ: {employee.Position}, Lương: {employee.Salary}"));
            }
            else
            {

                if (count == 99)
                {
                    Console.WriteLine("Danh sách trống");
                }
                count++;
            }

        }
        Console.WriteLine("--------------------------------");
    }

    public void AddEmployee()
    {
        // TODO: Yêu cầu người dùng nhập tên, tuổi, chức vụ và lương của nhân viên mới
        Console.WriteLine("Tạo một đối tượng Employee và thêm vào danh sách");
        // Tạo một đối tượng Employee và gọi hàm employeeManagement.AddEmployee();
        Employee inputEmployee = new Employee();
        Console.Write("Tên: ");
        inputEmployee.Name = Console.ReadLine();
        Console.Write("Tuổi: ");
        inputEmployee.Age = int.Parse(Console.ReadLine());
        Console.Write("Chức vụ: ");
        inputEmployee.Position = Console.ReadLine();
        Console.Write("Lương: ");
        inputEmployee.Salary = decimal.Parse(Console.ReadLine());
        employeeManagement.AddEmployee(inputEmployee.Name,
            inputEmployee.Age,
            inputEmployee.Position,
            inputEmployee.Salary);
        Console.WriteLine("Đã thêm thành công");
        Console.WriteLine("--------------------------------");
    }

    public void RemoveEmployee()
    {
        // TODO: Yêu cầu người dùng nhập tên của nhân viên cần xóa
        // gọi hàm employeeManagement.RemoveEmployee("hehehee");
        Console.Write("Nhập tên nhân viên muốn xóa: ");
        string inputName = Console.ReadLine();
        employeeManagement.RemoveEmployee(inputName);
        Console.WriteLine("Đã xóa thành công");
        Console.WriteLine("--------------------------------");
    }
}
