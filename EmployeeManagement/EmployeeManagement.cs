﻿public class EmployeeManagement
{
    Employee[] employees { get; set; }
    int count = 0;
    public EmployeeManagement()
    {
        employees = new Employee[100]; // Số nhân viên lưu trữ tối đa
    }

    public Employee[] GetEmployees()
    {
        // TODO: trả về thông tin của nhân viên trong hệ thống
        return employees.Where(x => x is not null).ToArray();
    }

    public void AddEmployee(string name, int age, string position, decimal salary)
    {
        // TODO: Viết logic để thêm nhân viên mới vào employees nhớ phải phân biệt nhau bằng ID       
        Employee newEmployee = new Employee
        {
            Id = count + 1,
            Name = name,
            Age = age,
            Position = position,
            Salary = salary
        };
        employees[count] = newEmployee;
    }

    public void RemoveEmployee(string name)
    {
        // TODO: Viết logic xóa nhân viên với tên
        int positionArray = 0;
        foreach (Employee employee in employees)
        {
            if (employee.Name.Equals(name))
            {
                positionArray = employee.Id - 1;
                break;
            }
        }
        employees[positionArray] = null;
        count--;
    }
}
